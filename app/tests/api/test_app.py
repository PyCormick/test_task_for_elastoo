from starlette.testclient import TestClient

from app.utils import Processing
from app.app import app


def test_get_item():
    with TestClient(app) as client:
        response = client.get("/items/")
        assert response.status_code == 200
