import csv
import re
from datetime import datetime

from dateutil.parser import isoparse, parse


# Вспомогательные функции

def detect_types(item_: str):
    """
    Функция для определения типов
    из полученных данных
    """
    try:
        pattern = r"\d{1,2}/\d{1,2}/\d{2,4}|" \
                  r"\d{2,4}/\d{1,2}/\d{1,2}|" \
                  r"\d{2,4}-\d{1,2}-\d{1,2}|" \
                  r"\d{1,2}-\d{1,2}-\d{2,4}|" \
                  r"\d{2,4}\.\d{1,2}\.\d{1,2}|" \
                  r"\d{1,2}\.\d{1,2}\.\d{2,4}"
        if re.fullmatch(pattern, item_):
            item_ = parse(item_).date()
        else:
            item_ = float(item_)
            if item_.is_integer():
                item_ = int(item_)
    except ValueError:
        try:
            item_ = isoparse(item_)
        except ValueError:
            item_ = str(item_)

    return item_


def get_value_in_columns(columns: [], query: str):
    """
    Функция для обработки данных
    в зависимости от полученного запроса(query)
    """
    if query == "max":
        return max(columns)
    elif query == "min":
        return min(columns)
    else:
        return [
            {"Error": "Query is don't supported"}
        ]


class Processing:
    def __init__(self, path_file: str):
        self.path_file = path_file
        self.list_items = []

    def get_items(self, column: str = None):
        self.list_items = []
        try:
            with open(self.path_file) as f_n:
                f_n_reader = csv.DictReader(f_n)
                if column:
                    self.list_items = list(map(detect_types,
                                               [row[column] for row in f_n_reader]))
                else:
                    self.list_items = list(f_n_reader)

            return self.list_items
        except KeyError:
            return [
                {"Error": "This column is not exist"}
            ]

    def processing_column(self, column: str,
                          query: str = None):
        if not query:
            columns = sorted(self.get_items(column), reverse=True)
            return columns
        else:
            columns = self.get_items(column)
            return get_value_in_columns(columns, query)


if __name__ == "__main__":
    items = Processing(path_file='MOCK_DATA.csv')
    items = items.processing_column("date", "msdf")
    print(items)
